<?php
  class Cooperativa extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("cooperativa",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $cooperativas=$this->db->get("cooperativa");
      if ($cooperativas->num_rows()>0) {
        return $cooperativas->result();
      } else {
        return false;
      }
    }
    //eliminacion de hospital por id
    function eliminar($id){
        $this->db->where("idco_gl",$id);
        return $this->db->delete("cooperativa");
    }
    //Consulta de un solo hospital
    function obtenerPorId($id){
      $this->db->where("idco_gl",$id);
      $cooperativa=$this->db->get("cooperativa");
      if ($cooperativa->num_rows()>0) {
        return $cooperativa->row();
      } else {
        return false;
      }
    }

    //function para actualizar hospitales
    function actualizar($id,$datos){
      $this->db->where("idco_gl",$id);
      return $this->db->update("cooperativa",$datos);
    }

  }//Fin de la clase
?>
