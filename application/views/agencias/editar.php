<h1>EDITAR AGENCIA</h1>
<form class="" method="post" action="<?php echo site_url('agencias/actualizarAgencia'); ?>"  id="frm_nuevo_agencia">

    <label for=""><b>TIPO COOPERATIVA:</b></label>
    <select class="form-control" name="idco_gl_cooperativa" id="idco_gl_cooperativa">
      <option value="">-- Seleccione la cooperativa --</option>
      <?php foreach ($cooperativas as $cooperativa) : ?>
          <option value="<?php echo $cooperativa->idco_gl; ?>" <?php if ($cooperativa->idco_gl == $agenciaEditar->idco_gl_cooperativa) echo 'selected'; ?>>
              <?php echo $cooperativa->nombre_gl; ?>
          </option>
      <?php endforeach; ?>
  </select>


    <input type="hidden" name="idage_gl" id="idage_gl" value="<?php echo $agenciaEditar->idage_gl; ?>"> <!-- Cerrar el input hidden -->
    <br>
    <label for=""><b>NOMBRE:</b></label>
    <input type="text" name="nombre_gl" id="nombre_gl" value="<?php echo $agenciaEditar->nombre_gl; ?>"  oninput="soloLetras(this)" placeholder="Ingrese el nombre..." class="form-control" required>
    <br>
    <label for=""><b>TELEFONO:</b></label>
    <input type="number" name="telefono_gl" id="telefono_gl" value="<?php echo $agenciaEditar->telefono_gl; ?>" oninput="soloNumeros(this)" placeholder="Ingrese el telefono..." class="form-control" required>
    <br>
    <label for=""><b>GERENTE:</b></label>
    <input type="text" name="gerente_gl" id="gerente_gl" value="<?php echo $agenciaEditar->gerente_gl; ?>" oninput="soloLetras(this)" placeholder="Ingrese el gerente..." class="form-control" required>

    <div class="row">
        <div class="col-md-6">
            <br>
            <label for=""><b>Latitud:</b></label>
            <input type="number" name="latitud_gl" id="latitud_gl" value="<?php echo $agenciaEditar->latitud_gl; ?>"  placeholder="Ingrese el latitud..." class="form-control" readonly>
        </div>
        <div class="col-md-6">
            <br>
            <label for=""><b>Longitud:</b></label>
            <input type="number" name="longitud_gl" id="longitud_gl" value="<?php echo $agenciaEditar->longitud_gl; ?>" placeholder="Ingrese el longitud..." class="form-control" readonly>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div id="mapa" style="height: 250px; width:100%; border:1px solid black;"></div> <!-- Corregir el ancho del mapa -->
        </div>
    </div>

    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-pen"></i> &nbsp Actualizar</button> &nbsp &nbsp
            <a href="<?php echo site_url('agencias/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>
        </div>
    </div>
</form>

<br>
<br>
<script type="text/javascript">
    function initMap(){
        var coordenadaCentral =
            new google.maps.LatLng(<?php echo $agenciaEditar->latitud_gl; ?>, <?php echo $agenciaEditar->longitud_gl; ?>);
        var miMapa= new google.maps.Map(
            document.getElementById('mapa'),{
                center: coordenadaCentral,
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
        );
        var marcador= new google.maps.Marker({
            position:coordenadaCentral,
            map: miMapa,
            title: 'Seleccione la ubicacion',
            draggable:true
        });
        google.maps.event.addListener(
            marcador,
            'dragend',
            function(event){
                var latitud=this.getPosition().lat();
                var longitud=this.getPosition().lng();
                document.getElementById('latitud_gl').value=latitud;
                document.getElementById('longitud_gl').value=longitud;
            }
        );
    }
    // Llama a la función initMap() al cargar la página
    initMap();
</script>


<script type="text/javascript">
    $("#frm_nuevo_agencia").validate({
        errorClass: 'text-danger',
        rules: {
          "idco_gl_cooperativa": {
              required: true
            },

            "nombre_gl": {
                required: true,
                minlength: 3, // Mínimo 3 caracteres
                maxlength: 50 // Máximo 50 caracteres
            },
            "telefono_gl": {
                required: true,
                minlength: 7, // Mínimo 7 dígitos
                maxlength: 15 // Máximo 15 dígitos
            },
            "gerente_gl": {
                required: true,
                minlength: 3, // Mínimo 3 caracteres
                maxlength: 50 // Máximo 50 caracteres
            },
            "latitud_gl": {
                required: true,
                number: true // Debe ser un número
            },
            "longitud_gl": {
                required: true,
                number: true // Debe ser un número
            }
        },
        messages: {
          "idco_gl_cooperativa": {
              required: '<span class="text-danger">Porfavor, selecione el tipo</span>',
          },
            "nombre_gl": {
                required: '<span class="text-danger">Debe ingresar el nombre</span>',
                minlength: '<span class="text-danger">El nombre debe tener al menos 3 caracteres</span>',
                maxlength: '<span class="text-danger">El nombre debe tener como máximo 50 caracteres</span>'
            },
            "telefono_gl": {
                required: '<span class="text-danger">Debe ingresar el teléfono</span>',
                minlength: '<span class="text-danger">El teléfono debe tener al menos 10 dígitos</span>',
                maxlength: '<span class="text-danger">El teléfono debe tener como máximo 15 dígitos</span>'
            },
            "gerente_gl": {
                required: '<span class="text-danger">Debe ingresar el nombre del gerente</span>',
                minlength: '<span class="text-danger">El nombre del gerente debe tener al menos 3 caracteres</span>',
                maxlength: '<span class="text-danger">El nombre del gerente debe tener como máximo 50 caracteres</span>'
            },
            "latitud_gl": {
                required: '<span class="text-danger">Debe ingresar la latitud</span>',
                number: '<span class="text-danger">La latitud debe ser un número</span>'
            },
            "longitud_gl": {
                required: '<span class="text-danger">Debe ingresar la longitud</span>',
                number: '<span class="text-danger">La longitud debe ser un número</span>'
            }
        }
    });
</script>

<script type="text/javascript">
    function soloNumeros(input) {
        // Reemplaza cualquier carácter que no sea un dígito numérico o un espacio con una cadena vacía
        input.value = input.value.replace(/[^\d\s]/g, '');
    }
</script>

<script type="text/javascript">
    function soloLetras(input) {
        // Reemplaza cualquier carácter que no sea una letra o un espacio con una cadena vacía
        input.value = input.value.replace(/[^a-zA-Z\s]/g, '');
    }
</script>
