
<h1>

<b>
  <i class="fa fa-plus-circle"></i>
  NUEVA AGENCIA
</b>
</h1>
<br>

<form class="" action="<?php echo site_url('agencias/guardarAgencia') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_agencia">
<label for=""> <b>TIPO COOPERATIVA: </b> </label>
<select data-live-search="true" data-live-search-style="startsWith" class="selectpicker" name="idco_gl_cooperativa" id="idco_gl_cooperativa" required>
    <option value="">--Seleccione la cooperativa--</option>
    <?php foreach ($cooperativa as $cooperativa): ?> <!-- Cambia $cooperativas a $cooperativa -->
        <option value="<?php echo $cooperativa->idco_gl; ?>"><?php echo $cooperativa->nombre_gl; ?></option>
    <?php endforeach; ?>
</select>


  <br>

 <label for=""> <b>NOMBRE: </b> </label>
 <input  id="nombre_gl" type="text" name="nombre_gl" value="" oninput="soloLetras(this)" placeholder="Ingrese el nombre" class="form-control" required>
 <br>
 <label for=""> <b>TELEFONO: </b> </label>
 <input  id="telefono_gl" type="number" name="telefono_gl" value="" oninput="soloNumeros(this)" placeholder="Ingrese el telefono" class="form-control" required>
 <br>
 <label for=""> <b>GERENTE: </b> </label>
 <input  id="gerente_gl" type="text" name="gerente_gl" value="" oninput="soloLetras(this)" placeholder="Ingrese el nombre del gerente" class="form-control" required>
 <br>
<div class="row">
  <div class="col-md-6">
    <label for=""> <b>LATITUD: </b> </label>
    <input  readonly id="latitud_gl" type="number" name="latitud_gl" value="" oninput="soloNumeros(this)" placeholder="Ingrese la latitud" class="form-control" required>
  </div>

  <div class="col-md-6">

    <label for=""> <b>LONGITUD: </b> </label>
    <input readonly id="longitud_gl" type="number" name="longitud_gl" value="" oninput="soloNumeros(this)" placeholder="Ingrese la longitud" class="form-control" required>

  </div>

  <div class="row">
    <div class="col-md-12">
      <br>
      <div id="mapa" style="height:250px; width:100%; border:1px solid black;" class="">

        <script type="text/javascript">
          function initMap(){
            var coordenadaCentral = new google.maps.LatLng(-0.18109812538860223, -78.47732204780648);
            var miMapa=new google.maps.Map(
              document.getElementById('mapa'),
              {
                center:coordenadaCentral,
                zoom:8,
                mapTypeId:google.maps.MapTypeId.ROADMAP
              }
            );

            var marcador=new google.maps.Marker({
              position:coordenadaCentral,
              map:miMapa,
              title:'Selecciona la ubicacion',
              draggable:true
            });

            google.maps.event.addListener(
              marcador,
              'dragend',
              function(event){
                var latitud=this.getPosition().lat();
                var longitud=this.getPosition().lng();
                //alert(latitud+"--"+longitud)
                document.getElementById('latitud_gl').value=latitud;
                document.getElementById('longitud_gl').value=longitud;
                              }
            );

          }

        </script>

      </div>

    </div>

  </div>

</div>
<br>
<div class="row">
  <div class="col-md-12 text-center">
    <button type="submit" name="button" class="btn btn-success"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspGuardar&nbsp</button>
    &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="btn btn-danger" href=" <?php echo site_url('agencias/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
  </div>

</div>

</form>
<br>

<script type="text/javascript">
    $("#frm_nuevo_agencia").validate({
        errorClass: 'text-danger',
        rules: {
          "idco_gl_cooperativa": {
              required: true
            },

            "nombre_gl": {
                required: true,
                minlength: 3, // Mínimo 3 caracteres
                maxlength: 50 // Máximo 50 caracteres
            },
            "telefono_gl": {
                required: true,
                minlength: 7, // Mínimo 7 dígitos
                maxlength: 15 // Máximo 15 dígitos
            },
            "gerente_gl": {
                required: true,
                minlength: 3, // Mínimo 3 caracteres
                maxlength: 50 // Máximo 50 caracteres
            },
            "latitud_gl": {
                required: true,
                number: true // Debe ser un número
            },
            "longitud_gl": {
                required: true,
                number: true // Debe ser un número
            }
        },
        messages: {
          "idco_gl_cooperativa": {
              required: '<span class="text-danger">Porfavor, selecione el tipo</span>',
          },
            "nombre_gl": {
                required: '<span class="text-danger">Debe ingresar el nombre</span>',
                minlength: '<span class="text-danger">El nombre debe tener al menos 3 caracteres</span>',
                maxlength: '<span class="text-danger">El nombre debe tener como máximo 50 caracteres</span>'
            },
            "telefono_gl": {
                required: '<span class="text-danger">Debe ingresar el teléfono</span>',
                minlength: '<span class="text-danger">El teléfono debe tener al menos 10 dígitos</span>',
                maxlength: '<span class="text-danger">El teléfono debe tener como máximo 15 dígitos</span>'
            },
            "gerente_gl": {
                required: '<span class="text-danger">Debe ingresar el nombre del gerente</span>',
                minlength: '<span class="text-danger">El nombre del gerente debe tener al menos 3 caracteres</span>',
                maxlength: '<span class="text-danger">El nombre del gerente debe tener como máximo 50 caracteres</span>'
            },
            "latitud_gl": {
                required: '<span class="text-danger">Debe ingresar la latitud</span>',
                number: '<span class="text-danger">La latitud debe ser un número</span>'
            },
            "longitud_gl": {
                required: '<span class="text-danger">Debe ingresar la longitud</span>',
                number: '<span class="text-danger">La longitud debe ser un número</span>'
            }
        }
    });
</script>

<script type="text/javascript">
    function soloNumeros(input) {
        // Reemplaza cualquier carácter que no sea un dígito numérico o un espacio con una cadena vacía
        input.value = input.value.replace(/[^\d\s]/g, '');
    }
</script>

<script type="text/javascript">
    function soloLetras(input) {
        // Reemplaza cualquier carácter que no sea una letra o un espacio con una cadena vacía
        input.value = input.value.replace(/[^a-zA-Z\s]/g, '');
    }
</script>
