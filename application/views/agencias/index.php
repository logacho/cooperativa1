
<h1 style="text-align: center; color: #0D47A1;">
    <i class="fas fa-building"></i> AGENCIAS
</h1>





<div class="row mt-4">
  <div class="col-md-12 text-end">
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      <i class="fa fa-eye"></i> Ver Mapa
    </button>
    <a class="btn btn-outline-success" href="<?php echo site_url('agencias/nuevo') ?>">
      <i class="fa fa-plus-circle"></i> Agregar agencia
    </a>
  </div>
</div>
<br>

<?php if ($listadoAgencias): ?>
  <table class="table table-bordered table-striped table-hover">
      <thead>
          <tr class="bg-primary text-white">
              <th>ID</th>
              <th>NOMBRE</th>
              <th>TELÉFONO</th>
              <th>GERENTE</th>
              <th>LATITUD</th>
              <th>LONGITUD</th>
              <th>TIPO COOPERATIVA</th>
              <th>ACCIONES</th>
          </tr>
      </thead>
      <tbody>
          <?php foreach ($listadoAgencias as $agencia): ?>
              <tr>
                  <td><?php echo $agencia->idage_gl; ?></td>
                  <td><?php echo $agencia->nombre_gl; ?></td>
                  <td><?php echo $agencia->telefono_gl; ?></td>
                  <td><?php echo $agencia->gerente_gl; ?></td>
                  <td><?php echo $agencia->latitud_gl; ?></td>
                  <td><?php echo $agencia->longitud_gl; ?></td>
                  <td><?php echo $agencia->idco_gl_cooperativa; ?></td>
                  <td>
                      <a href="<?php echo site_url('agencias/editar/') . $agencia->idage_gl; ?>" class="btn btn-warning" title="Editar">
                          <i class="fa fa-pen"></i>
                      </a>
                      <a href="<?php echo site_url('agencias/borrar/') . $agencia->idage_gl; ?>" class="btn btn-danger" title="Eliminar">
                          <i class="fa fa-trash"></i>
                      </a>

                  </td>
              </tr>
          <?php endforeach; ?>
      </tbody>
  </table>


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-eye"></i> Mapa de Agencias</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div id="reporteMapa" style="height:300px; width:100%; border:2px solid black;"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function initMap() {
            var coordenadaCentral = new google.maps.LatLng(-0.152948869329262, -78.4868431364856);
            var miMapa = new google.maps.Map(
                document.getElementById('reporteMapa'), {
                    center: coordenadaCentral,
                    zoom: 8,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
            );
            <?php foreach ($listadoAgencias as $agencia): ?>
                var coordenadaTemporal = new google.maps.LatLng(<?php echo $agencia->latitud_gl; ?>, <?php echo $agencia->longitud_gl; ?>);
                var marcador = new google.maps.Marker({
                    position: coordenadaTemporal,
                    map: miMapa,
                    title: '<?php echo $agencia->nombre_gl; ?>',
                });
            <?php endforeach; ?>
        }
    </script>

<?php else: ?>
    <div class="alert alert-danger">
        No se encontraron agencias registradas
    </div>
<?php endif; ?>

<script type="text/javascript">
  $(document).ready(function() {
    // Inicializa DataTable con el archivo de idioma español y botones
    $('#tbl_agencia').DataTable({
      dom: 'Bfrtip',
      buttons: [
        { extend: 'print', className: 'btn btn-primary', text: 'Imprimir' },
        { extend: 'pdf', className: 'btn btn-danger', text: 'PDF' },
        { extend: 'csv', className: 'btn btn-warning', text: 'CSV' }
      ],
      language: {
        url: '//cdn.datatables.net/plug-ins/1.13.7/i18n/Spanish.json' // Cambia la URL del archivo de idioma a Spanish.json
      }
    });
  });
</script>
