<div class="container-fluid p-0 mb-5 wow fadeIn" data-wow-delay="0.1s">
		<div id="header-carousel" class="carousel slide" data-bs-ride="carousel">
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img class="w-100" src="<?php echo base_url('static/img/agencia.jpg'); ?>" />
					<div class="carousel-caption">
						<div class="container">
							<div class="row">
								<div class="col-12 col-lg-6">
									<h1 class="display-3 text-dark mb-4 animated slideInDown">
									</h1>
									<p class="fs-5 text-body mb-5">
									</p>
									<a href="" class="btn btn-primary py-3 px-5"
										></a
									>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="carousel-item">
					<img class="w-100" src="<?php echo base_url('static/img/financiamiento.jpg'); ?>" />
					<div class="carousel-caption">
						<div class="container">
							<div class="row">
								<div class="col-12 col-lg-6">
									<h1 class="display-3 text-dark mb-4 animated slideInDown">
										SOMOS LA COOPERATIVA CON MAYOR FINANCIMIENTO DEL EXTERIOR
									</h1>
									<p class="fs-5 text-body mb-5">
										Gracias a nuestro excelentes resultados
										recibimos mas de  USD 130 millones del
										exterior.
									</p>
									<a href="" class="btn btn-primary py-3 px-5"
										></a
									>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<button
				class="carousel-control-prev"
				type="button"
				data-bs-target="#header-carousel"
				data-bs-slide="prev"
			>
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="visually-hidden">Previous</span>
			</button>
			<button
				class="carousel-control-next"
				type="button"
				data-bs-target="#header-carousel"
				data-bs-slide="next"
			>
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="visually-hidden">Next</span>
			</button>
		</div>
	</div>
	<!-- Carousel End -->
	<!-- Service Start -->
	    <div class="container-xxl py-5">
	      <div class="container">
	        <div class="text-center mx-auto" style="max-width: 500px">
	          <h1 class="display-6 mb-5">
	          MISION Y VISION DE LA COOPERATIVA
	          </h1>
	        </div>
	        <div class="row g-4 justify-content-center">
	          <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
	            <div class="service-item rounded h-100 p-5">
	              <div class="d-flex align-items-center ms-n5 mb-4">
	                <div
	                  class="service-icon flex-shrink-0 bg-primary rounded-end me-4"
	                >
	                  <img
	                    class="img-fluid"
	                    src="<?php echo base_url('static/img/icon/icon-10-light.png'); ?>"
	                    alt=""
	                  />
	                </div>
	                <h4 class="mb-0">MISION</h4>
	              </div>
	              <p class="mb-4">
	               Somos una Cooperativa de Ahorro y Crédito inclusiva que ofrece productos y servicios
								 financieros que contribuyen al desarrollo económico de sus socios y clientes, sustentados
								 en los principios cooperativos.
	              </p>
	              <a class="btn btn-light px-3" href="<?php echo site_url('cooperativas/index') ?>">EDITAR</a>
	            </div>
	          </div>
	          <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
	            <div class="service-item rounded h-100 p-5">
	              <div class="d-flex align-items-center ms-n5 mb-4">
	                <div
	                  class="service-icon flex-shrink-0 bg-primary rounded-end me-4"
	                >
	                  <img
	                    class="img-fluid"
											src="<?php echo base_url('static/img/icon/icon-05-light.png'); ?>"
	                    alt=""
	                  />
	                </div>
	                <h4 class="mb-0">VISION</h4>
	              </div>
	              <p class="mb-4">
	               Ser reconocida entre las mejores entidades financieras a nivel nacional por ofertar productos y servicios innovadores
								 con enfoque digital y altos estándares de experiencia y satisfacción en el servicio.
	              </p>
	              <a class="btn btn-light px-3" href="<?php echo site_url('cooperativas/index') ?>">EDITAR</a>
	            </div>
	          </div>

	    </div>
			<!-- About Start -->
     <div class="container-xxl py-5">
       <div class="container">
         <div class="row g-5">
           <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
             <div
               class="position-relative overflow-hidden rounded ps-5 pt-5 h-100"
               style="min-height: 400px"
             >
               <img
                 class="position-absolute w-100 h-100"
								 src="<?php echo base_url('static/img/cooperativa.png'); ?>"
                 alt=""
                 style="object-fit: cover"
               />
               <div
                 class="position-absolute top-0 start-0 bg-white rounded pe-3 pb-3"
                 style="width: 200px; height: 200px"
               >
                 <div
                   class="d-flex flex-column justify-content-center text-center bg-primary rounded h-100 p-3"
                 >
                   <h1 class="text-white mb-0">53 años</h1>
                   <h2 class="text-white">de</h2>
                   <h5 class="text-white mb-0">vida institucional</h5>
                 </div>
               </div>
             </div>
           </div>
           <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.5s">
             <div class="h-100">
               <h1 class="display-6 mb-5">
                HISTORIA
               </h1>
               <div class="row g-4 mb-4">
                 <div class="col-sm-6">
                   <div class="d-flex align-items-center">
                     <img
                       class="flex-shrink-0 me-3"
                       src="img/icon/icon-04-primary.png"
                       alt=""
                     />
                   </div>
                 </div>
                 <div class="col-sm-6">
                   <div class="d-flex align-items-center">
                     <img
                       class="flex-shrink-0 me-3"
                       src="img/icon/icon-03-primary.png"
                       alt=""
                     />
                   </div>
                 </div>
               </div>
               <p class="mb-4">
								 Nuestra historia empieza en el año 1969, cuando moradores del barrio Chaupitena ubicado en el Valle de los Chillos,
								 identificaron la necesidad de crear una Institución que apoye a la comunidad en sus proyectos y tenga una visión
								 solidaria, por ello inicia la idea de realizar una cooperativa de ahorro para captar el dinero, capitalizarlo y
								 brindar Crédito a todos y cada uno de sus asociados.
                 Sus mentalistas fueron un grupo de jóvenes visionarios, con el objetivo de mejorar la situación económica
								 por la cual atravesaban los pobladores de la zona, brindar respaldo y apoyo.
               </p>
							 <div class="border-top mt-4 pt-4">
								 <div class="d-flex align-items-center">
									 <button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo site_url('cooperativas/index'); ?>'">EDITAR</button>
								 </div>

							 </div>

             </div>
           </div>
         </div>
       </div>
     </div>
     <!-- About End -->
	    <!-- Service End -->
