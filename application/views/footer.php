<!-- Footer Start -->
<div class="container-fluid bg-primary text-dark footer mt-5 pt-3 wow fadeIn" style="background-color: #007bff;" data-wow-delay="0.1s">

    <div class="container py-2">
        <div class="row g-4">
            <div class="col-lg-3 col-md-6">
                <h5 class="text-light mb-3">
                    <img class="img-fluid me-3" src="img/icon/icon-02-light.png" alt="" />Nosotros
                </h5>
                <hr class="text-light mb-3">
                <h6 class="text-light">Noticias Alianza del Valle</h6>
                <h6 class="text-light">Alianza Sostenible</h6>
                <h6 class="text-light">Transparencia de la Informacion</h6>
                <h6 class="text-light">Normas de Seguridad</h6>
            </div>
            <div class="col-lg-3 col-md-6">

                <h6 class="text-light mb-3">Trabaja con nosotros</h6>
                <h6 class="text-light">Facturacion Electronica</h6>
                <h6 class="text-light">Internet</h6>
            </div>
            <div class="col-lg-3 col-md-6">
                <h5 class="text-light mb-3">Contactanos</h5>
                <hr class="text-light mb-3">
                <div class="d-flex">
                    <i class="fas fa-phone-alt me-2"></i>
                    <h6 class="text-light mb-0">Teléfono: (02) 299 8600</h6>
                </div>
                <div class="d-flex">
                    <i class="fab fa-whatsapp me-2"></i>
                    <h6 class="text-light mb-0">(09) 90328625 WhatsApp </h6>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
                    &copy; <a href="#" class="text-light">Your Site Name</a>, All Right Reserved.
                </div>
                <div class="col-md-6 text-center text-md-end">
                    Designed By <a href="https://htmlcodex.com" class="text-light">HTML Codex</a> <br />Distributed By: <a
                        href="https://themewagon.com" target="_blank" class="text-light">ThemeWagon</a>
                </div>
            </div>
        </div>
    </div>
</div>


   <!-- Footer End -->

<!-- JavaScript Libraries -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url('static/lib/wow/wow.min.js'); ?>"></script>
<script src="<?php echo base_url('static/lib/easing/easing.min.js'); ?>"></script>
<script src="<?php echo base_url('static/lib/waypoints/waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('static/lib/owlcarousel/owl.carousel.min.js'); ?>"></script>
<script src="<?php echo base_url('static/lib/counterup/counterup.min.js'); ?>"></script>

<!-- Template Javascript -->
<script src="<?php echo base_url('static/js/main.js'); ?>"></script>


</body>
</html>
