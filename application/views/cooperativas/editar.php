<h1 style="text-align: center; color: #1E3A5F;">
    <i class="fas fa-edit" style="color: #1E3A5F;"></i>
    EDITAR COOPERATIVA
</h1>

<form class="" method="post" action="<?php echo site_url('cooperativas/actualizarCooperativa'); ?>">


  <input type="hidden" name="idco_gl" id="idco_gl"
	value="<?php echo $cooperativaEditar->idco_gl; ?>">
  <label for="">
    <b>NOMBRE:</b>
  </label>
  <input type="text" name="nombre_gl" id="nombre_gl"
	value="<?php echo $cooperativaEditar->nombre_gl; ?>"
  placeholder="Ingrese el nombre..." class="form-control" required>
  <br>
  <label for="">
    <b>TELEFONO:</b>
  </label>
  <input type="number" name="telefono_gl" id="telefono_gl"
	value="<?php echo $cooperativaEditar->telefono_gl; ?>"
  placeholder="Ingrese el numero telefono..." class="form-control" required>
  <br>
  <label for="">
    <b>CORREO:</b>
  </label>
  <input type="text" name="correo_gl" id="correo_gl"
	value="<?php echo $cooperativaEditar->correo_gl; ?>"
  placeholder="Ingrese el correo..." class="form-control" required>

  <label for="mision_gl"><b>MISION:</b></label>
  <textarea id="mision_gl" name="mision_gl" placeholder="Ingrese la misión..." class="form-control" required><?php echo $cooperativaEditar->mision_gl; ?></textarea>
  <br>

  <label for="vision_gl"><b>VISION:</b></label>
  <textarea id="vision_gl" name="vision_gl" placeholder="Ingrese la visión..." class="form-control" required><?php echo $cooperativaEditar->vision_gl; ?></textarea>
  <br>

  <label for="historia_gl"><b>HISTORIA:</b></label>
  <textarea id="historia_gl" name="historia_gl" placeholder="Ingrese la historia..." class="form-control" required><?php echo $cooperativaEditar->historia_gl; ?></textarea>
  <br>



    <div class="row">
      <div class="col-md-6">
        <br>
        <label for="">
        <b>LATITUD:</b>
      </label>
      <input type="number" name="latitud_gl" id="latitud_gl"
			value="<?php echo $cooperativaEditar->latitud_gl; ?>"
      placeholder="Ingrese el latitud..." class="form-control" readonly>

      </div>
      <div class="col-md-6">
        <br>
        <label for="">
        <b>LONGITUD:</b>
      </label>
      <input type="number" name="longitud_gl" id="longitud_gl"
			value="<?php echo $cooperativaEditar->longitud_gl; ?>"
      placeholder="Ingrese el longitud..." class="form-control" readonly>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div id="mapa" style="height: 250px; whidth:100%; border:1px solid black;">

      </div>
      </div>

    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-pen"></i> &nbsp Actualizar</button> &nbsp &nbsp
        <a href="<?php echo site_url('cooperativas/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>

      </div>

    </div>
</form>

<br>
<br>
<script type="text/javascript">
    function initMap(){
        var coordenadaCentral =
            new google.maps.LatLng(<?php echo $cooperativaEditar->latitud_gl; ?>, <?php echo $cooperativaEditar->longitud_gl; ?>);
        var miMapa= new google.maps.Map(
            document.getElementById('mapa'),{
                center: coordenadaCentral,
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
        );
        var marcador= new google.maps.Marker({
            position:coordenadaCentral,
            map: miMapa,
            title: 'Seleccione la ubicacion',
            draggable:true
        });
        google.maps.event.addListener(
            marcador,
            'dragend',
            function(event){
                var latitud=this.getPosition().lat();
                var longitud=this.getPosition().lng();
                document.getElementById('latitud_gl').value=latitud;
                document.getElementById('longitud_gl').value=longitud;
            }
        );
    }
    // Llama a la función initMap() al cargar la página
    initMap();
</script>
