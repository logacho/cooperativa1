<h1 class="text-center" style="color: #004080;">
    <i class="fas fa-handshake"></i> COOPERATIVA
</h1>



<div class="row">
  <div class="col-md-12 text-end">
    <!-- Botón para abrir el modal -->
    <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      <i class="fa fa-eye"></i> Ver Mapa
    </button>

    <!-- Botón para agregar una nueva cooperativa -->
    <a class="btn btn-outline-success" href="<?php echo site_url('cooperativas/nuevo') ?>"> <i class="fa fa-plus-circle"></i>
      Agregar cooperativa</a>
  </div>
</div>
<br>

<?php if ($listadoCooperativas): ?>
  <table class="table table-bordered table-striped" style="background-color: #f8f9fa;">
  <thead class="bg-primary text-white">
      <tr>
          <th>ID</th>
          <th>NOMBRE</th>
          <th>TELÉFONO</th>
          <th>CORREO</th>
          <th>LATITUD</th>
          <th>LONGITUD</th>
          <th>MISIÓN</th>
          <th>VISIÓN</th>
          <th>HISTORIA</th>
          <th>ACCIONES</th>
      </tr>
  </thead>
  <tbody>
      <?php foreach ($listadoCooperativas as $cooperativa): ?>
          <tr>
              <td><?php echo $cooperativa->idco_gl; ?></td>
              <td><?php echo $cooperativa->nombre_gl; ?></td>
              <td><?php echo $cooperativa->telefono_gl; ?></td>
              <td><?php echo $cooperativa->correo_gl; ?></td>
              <td><?php echo $cooperativa->latitud_gl; ?></td>
              <td><?php echo $cooperativa->longitud_gl; ?></td>
              <td><?php echo $cooperativa->mision_gl; ?></td>
              <td><?php echo $cooperativa->vision_gl; ?></td>
              <td><?php echo $cooperativa->historia_gl; ?></td>
              <td>
                  <a href="<?php echo site_url('cooperativas/editar/').$cooperativa->idco_gl; ?>" class="btn btn-warning" title="Editar">
                      <i class="fa fa-pen"></i>
                  </a>
                  <a href="<?php echo site_url('cooperativas/borrar/').$cooperativa->idco_gl; ?>" class="btn btn-danger" title="Eliminar">
                      <i class="fa fa-trash"></i>
                  </a>
              </td>
          </tr>
      <?php endforeach; ?>
  </tbody>
</table>


    <!-- Modal para mostrar el mapa -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"> <i class="fa fa-eye"></i> Mapa de Cooperativas </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div id="reporteMapa" style="height:300px; width:100%; border:2px solid black;"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-bs-dismiss="modal"> <i class="fa fa-times"></i> Cerrar</button>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      function initMap(){
        var coordenadaCentral = new google.maps.LatLng(-0.152948869329262, -78.4868431364856);
        var miMapa = new google.maps.Map(
          document.getElementById('reporteMapa'),
          {
            center: coordenadaCentral,
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          }
        );
        <?php foreach ($listadoCooperativas as $cooperativa): ?>
          var coordenadaTemporal = new google.maps.LatLng(<?php echo $cooperativa->latitud_gl; ?>, <?php echo $cooperativa->longitud_gl; ?>);
          var marcador = new google.maps.Marker({
            position: coordenadaTemporal,
            map: miMapa,
            title: '<?php echo $cooperativa->nombre_gl; ?>'
          });
        <?php endforeach; ?>
      }
    </script>
<?php else: ?>
  <div class="alert alert-danger"> No se encontraron cooperativas registradas </div>
<?php endif; ?>
