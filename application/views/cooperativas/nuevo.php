<h1 style="text-align: center;">
    <b style="color: blue;">
        <i class="fa fa-handshake"></i>
        NUEVA COOPERATIVA
    </b>
</h1>

<br>

<form class="" action="<?php echo site_url('cooperativas/guardarCooperativa') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_cooperativa">



 <label for=""> <b>NOMBRE: </b> </label>
 <input  id="nombre_gl" type="text" name="nombre_gl" value="" oninput="soloLetras(this)" placeholder="Ingrese el nombre" class="form-control" required>
 <br>
 <label for=""> <b>TELEFONO: </b> </label>
 <input  id="telefono_gl" type="number" name="telefono_gl" value="" oninput="soloNumeros(this)"  placeholder="Ingrese el telefono" class="form-control" required>
 <br>
 <label for=""> <b>CORREO: </b> </label>
 <input  id="correo_gl" type="text" name="correo_gl" value="" placeholder="Ingrese el correo" class="form-control" required>
 <br>
 <label for="mision_gl"><b>MISION: </b></label>
 <textarea id="mision_gl" name="mision_gl" placeholder="Ingrese la misión" oninput="soloLetras(this)" class="form-control" required></textarea>
 <br>
 <label for="vision_gl"><b>VISION: </b></label>
 <textarea id="vision_gl" name="vision_gl" placeholder="Ingrese la vision" oninput="soloLetras(this)" class="form-control" required></textarea>
 <br>
 <label for="mision_gl"><b>HISTORIA: </b></label>
 <textarea id="historia_gl" name="historia_gl" placeholder="Ingrese la historia" class="form-control" required></textarea>
 <br>

<div class="row">
  <div class="col-md-6">
    <label for=""> <b>LATITUD: </b> </label>
    <input  readonly id="latitud_gl" type="number" name="latitud_gl" value="" placeholder="Ingrese la latitud" class="form-control" required>
  </div>

  <div class="col-md-6">

    <label for=""> <b>LONGITUD: </b> </label>
    <input readonly id="longitud_gl" type="number" name="longitud_gl" value="" placeholder="Ingrese la longitud" class="form-control" required>

  </div>


  <div class="row">
    <div class="col-md-12">
      <br>
      <div id="mapa" style="height:250px; width:100%; border:1px solid black;" class="">

        <script type="text/javascript">
          function initMap(){
            var coordenadaCentral = new google.maps.LatLng(-0.18109812538860223, -78.47732204780648);
            var miMapa=new google.maps.Map(
              document.getElementById('mapa'),
              {
                center:coordenadaCentral,
                zoom:8,
                mapTypeId:google.maps.MapTypeId.ROADMAP
              }
            );

            var marcador=new google.maps.Marker({
              position:coordenadaCentral,
              map:miMapa,
              title:'Selecciona la ubicacion',
              draggable:true
            });

            google.maps.event.addListener(
              marcador,
              'dragend',
              function(event){
                var latitud=this.getPosition().lat();
                var longitud=this.getPosition().lng();
                //alert(latitud+"--"+longitud)
                document.getElementById('latitud_gl').value=latitud;
                document.getElementById('longitud_gl').value=longitud;
                              }
            );

          }

        </script>

      </div>

    </div>

  </div>

</div>
<br>
<div class="row">
  <div class="col-md-12 text-center">
    <button type="submit" name="button" class="btn btn-success"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspGuardar&nbsp</button>
    &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="btn btn-danger" href=" <?php echo site_url('cooperativas/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
  </div>

</div>

</form>
<br>

<script type="text/javascript">
    $("#frm_nuevo_cooperativa").validate({
        rules: {
            "nombre_gl": {
                required: true,
                minlength: 3, // Ejemplo: mínimo 3 caracteres
                maxlength: 50 // Ejemplo: máximo 50 caracteres
            },
            "telefono_gl": {
                required: true,
                minlength: 7, // Ejemplo: mínimo 7 dígitos
                maxlength: 15 // Ejemplo: máximo 15 dígitos
            },
            "correo_gl": {
                required: true,
                email: true // Validar formato de correo electrónico
            },
            "mision_gl": {
                required: true
            },
            "vision_gl": {
                required: true
            },
            "historia_gl": {
                required: true
            },
            "latitud_gl": {
                required: true
            },
            "longitud_gl": {
                required: true
            }
        },
        messages: {
            "nombre_gl": {
                required: "Debe ingresar el nombre",
                minlength: "El nombre debe tener al menos {0} caracteres",
                maxlength: "El nombre debe tener como máximo {0} caracteres"
            },
            "telefono_gl": {
                required: "Debe ingresar el teléfono",
                minlength: "El teléfono debe tener al menos {0} dígitos",
                maxlength: "El teléfono debe tener como máximo {0} dígitos"
            },
            "correo_gl": {
                required: "Debe ingresar el correo",
                email: "Debe ingresar un correo electrónico válido"
            },
            "mision_gl": {
                required: "Debe ingresar la misión"
            },
            "vision_gl": {
                required: "Debe ingresar la visión"
            },
            "historia_gl": {
                required: "Debe ingresar la historia"
            },
            "latitud_gl": {
                required: "Debe ingresar la latitud"
            },
            "longitud_gl": {
                required: "Debe ingresar la longitud"
            }
        },
        errorClass: 'text-danger'
    });
</script>

<script type="text/javascript">
    function soloNumeros(input) {
        // Reemplaza cualquier carácter que no sea un dígito numérico o un espacio con una cadena vacía
        input.value = input.value.replace(/[^\d\s]/g, '');
    }
</script>

<script type="text/javascript">
    function soloLetras(input) {
        // Reemplaza cualquier carácter que no sea una letra o un espacio con una cadena vacía
        input.value = input.value.replace(/[^a-zA-Z\s]/g, '');
    }
</script>
