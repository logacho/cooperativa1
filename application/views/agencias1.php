<div class="container">
    <div class="row">
        <!-- Columna para el listado de agencias -->
        <div class="col-md-4" style="border: 2px solid #1e2a78; padding: 10px; border-radius: 10px;">
            <h2 style="color: #FFC107; text-align: center;">
                <i class="fas fa-map-marked-alt"></i> Listado de Agencias
            </h2>

            <ul>
                <?php foreach ($listadoAgencias as $agencia) : ?>
                    <li><?php echo $agencia->nombre_gl; ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <!-- Columna para el mapa de agencias -->
        <div class="col-md-6">
            <h2 style="color: #FF5722; text-align: center;">
                <i class="fas fa-map"></i> Mapa de Agencias
            </h2>

            <div id="map" style="height: 400px;"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function initMap() {
        var coordenadaCentral = new google.maps.LatLng(-0.152948869329262, -78.4868431364856);
        var miMapa = new google.maps.Map(
            document.getElementById('map'), {
                center: coordenadaCentral,
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
        );
        <?php foreach ($listadoAgencias as $agencia): ?>
            var coordenadaTemporal = new google.maps.LatLng(<?php echo $agencia->latitud_gl; ?>, <?php echo $agencia->longitud_gl; ?>);
            var marcador = new google.maps.Marker({
                position: coordenadaTemporal,
                map: miMapa,
                title: '<?php echo $agencia->nombre_gl; ?>',
                icon: '<?php echo base_url('static/img/logocoperativa.ico'); ?>'
            });
        <?php endforeach; ?>
    }
    // Llama a la función initMap() al cargar la página
    initMap();
</script>
