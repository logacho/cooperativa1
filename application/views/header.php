<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>ALIANZA DEL VALLE </title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="keywords" />
    <meta content="" name="description" />

    <!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Google Maps API -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxKneyvn8b9jwvDdDC0EW7IfJMZ5R6At8&libraries=places&callback=initMap"></script>
<!-- SweetAlert -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.3/dist/sweetalert2.all.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.3/dist/sweetalert2.min.css" />

<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>

<!-- jQuery Validate -->
<script type="text/javascript" src="<?php echo base_url('static/lib/jquery-validate/jquery.validate.js'); ?>"></script>


<!-- Favicon -->
<link href="<?php echo base_url('static/img/favicon.ico'); ?>" rel="icon" />

<!-- Google Web Fonts -->
<link rel="preconnect" href="https://fonts.googleapis.com" />
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&family=Poppins:wght@600;700&display=swap" rel="stylesheet" />

<!-- Icon Font Stylesheet -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet" />

<!-- Libraries Stylesheet -->
<link href="<?php echo base_url('static/lib/animate/animate.min.css'); ?>" rel="stylesheet" />
<link href="<?php echo base_url('static/lib/owlcarousel/assets/owl.carousel.min.css'); ?>" rel="stylesheet" />

<!-- Template Stylesheet -->
<link href="<?php echo base_url('static/css/style.css'); ?>" rel="stylesheet" />



<!-- Customized Bootstrap Stylesheet -->
<link href="<?php echo base_url('static/css/bootstrap.min.css'); ?>" rel="stylesheet" />


<!-- DataTables JS -->
<script type="text/javascript" src="//cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
<!-- DataTables CSS -->
<link rel="stylesheet" href="//cdn.datatables.net/1.13.7/css/jquery.dataTables.min.css">


  </script>




  </head>

  <body>
    <!-- Spinner Start -->
    <div
      id="spinner"
      class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center"
    >
      <div class="spinner-grow text-primary" role="status"></div>
    </div>
    <!-- Spinner End -->

    <!-- Topbar Start -->
    <div
      class="container-fluid bg-dark text-white-50 py-2 px-0 d-none d-lg-block"
    >
      <div class="row gx-0 align-items-center">
        <div class="col-lg-7 px-5 text-start">
          <div class="h-100 d-inline-flex align-items-center me-4">
            <small class="fa fa-phone-alt me-2"></small>
            <small>(02) 299 8600</small>
          </div>
          <div class="h-100 d-inline-flex align-items-center me-4">
            <small class="far fa-envelope-open me-2"></small>
            <small> info@alianzadelvalle.fin.ec</small>
          </div>
        </div>
        <div class="col-lg-5 px-5 text-end">
          <div class="h-100 d-inline-flex align-items-center">
            <a class="text-white-50 ms-4" href=""
              ><i class="fab fa-facebook-f"></i
            ></a>
            <a class="text-white-50 ms-4" href=""
              ><i class="fab fa-twitter"></i
            ></a>
            <a class="text-white-50 ms-4" href=""
              ><i class="fab fa-linkedin-in"></i
            ></a>
            <a class="text-white-50 ms-4" href=""
              ><i class="fab fa-instagram"></i
            ></a>
          </div>
        </div>
      </div>
    </div>
    <!-- Topbar End -->

    <!-- Navbar Start -->
    <nav
      class="navbar navbar-expand-lg bg-white navbar-light sticky-top px-4 px-lg-5"
    >
      <a href="index.html" class="navbar-brand d-flex align-items-center">
        <h1 class="m-0">
          <img
            class="static/img-fluid me-3"
            src="<?php echo base_url('static/img/logocop.jpeg'); ?>"
            alt=""
          />
        </h1>
      </a>
      <button
        type="button"
        class="navbar-toggler"
        data-bs-toggle="collapse"
        data-bs-target="#navbarCollapse"
      >
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <div class="navbar-nav mx-auto bg-light rounded pe-4 py-3 py-lg-0">
          <a  href="<?php echo site_url('/');?>" class="nav-item nav-link active">Inicio</a>
          <a href="<?php echo site_url('agencias/mostrar_mapa');?>" class="nav-item nav-link">Listado Agencias</a>
          <a href="service.html" class="nav-item nav-link">Nosotros</a>
          <a href="service.html" class="nav-item nav-link">Ponte en contacto</a>
          <div class="nav-item dropdown">
            <a
              href="#"
              class="nav-link dropdown-toggle"
              data-bs-toggle="dropdown"
              >Pages</a
            >
            <div class="dropdown-menu bg-light border-0 m-0">
              <a href="<?php echo site_url('agencias/index'); ?>" class="dropdown-item">AGENCIAS</a>
              <a href="<?php echo site_url('cooperativas/index') ?>" class="dropdown-item">COOPERATIVA</a>
              

            </div>
          </div>
        </div>
      </div>
      <a href="" class="btn btn-primary px-3 d-none d-lg-block">COOPERATIVA EMPRESAS</a>
    </nav>
    <!-- Navbar End -->
    <?php if ($this->session->flashdata('confirmacion')): ?>
              <script type="text/javascript">
                Swal.fire({
                  title: "CONFIRMACION",
                  text: "<?php echo $this->session->flashdata('confirmacion'); ?>",
                  icon: "success" // error, warning, info
                  });
              </script>
              <?php $this->session->set_flashdata('confirmacion',''); ?>
            <?php endif; ?>

            <?php if ($this->session->flashdata('eliminacion')): ?>
              <script type="text/javascript">
                Swal.fire({
                  title: "CONFIRMACION",
                  text: "<?php echo $this->session->flashdata('eliminacion'); ?>",
                  icon: "error" // error, warning, info
                  });
              </script>
              <?php $this->session->set_flashdata('eliminacion',''); ?>
            <?php endif; ?>
