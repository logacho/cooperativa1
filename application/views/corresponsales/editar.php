<h1>EDITAR CORRESPONSAL</h1>
<form class="" method="post" action="<?php echo site_url('corresponsales/actualizarCorresponsal'); ?>">
	<input type="hidden" name="idcor_gl" id="idcor_gl"
	value="<?php echo $corresponsalEditar->idcor_gl; ?>">
	<label for="">AGENCIAS</label>
  <select name="idage_gl_agencia" id="idage_gl_agencia" required class="form-control">
    <option value="">Seleccione la Agencia</option>
    <?php foreach ($agencias as $agencia) : ?>
      <option value="<?php echo $agencia->idage_gl; ?>">
        <?php echo $agencia->nombre_gl; ?>
      </option>
    <?php endforeach; ?>
  </select>
  <label for="">
    <b>Nombre:</b>
  </label>
  <input type="text" name="nombre_gl" id="nombre_gl"
	value="<?php echo $corresponsalEditar->nombre_gl; ?>"
  placeholder="Ingrese el nombre..." class="form-control" required
	oninput="this.value = this.value.replace(/[^a-zA-Z\s]/g, '')">
  <br>
  <label for="">
    <b>SERVICIOS:</b>
  </label>
  <input type="text" name="servicios_gl" id="servicios_gl"
	value="<?php echo $corresponsalEditar->servicios_gl; ?>"
  placeholder="Ingrese el nombre..." class="form-control" required
	oninput="this.value = this.value.replace(/[^a-zA-Z\s]/g, '')">
  <br>
	<div class="row ">
    <div class="col-md-6 mb-4">
      <label class="">Foto Actual</label>
      <br>
      <?php if (!empty($corresponsalEditar->foto_gl)): ?>
        <a target="_blank" href="<?php echo base_url('uploads/corresponsales/') . $corresponsalEditar->foto_gl; ?>">
            <img width="150" src="<?php echo base_url('uploads/corresponsales/') . $corresponsalEditar->foto_gl; ?>" alt="">
          </a>
        <?php else: ?>
            <p>No hay foto</p>
      <?php endif ?>
    </div>
    <div class="col-md-6">
      <label class="">Cambiar Foto</label>
      <br>
        <input
          class="form-control"
          type="file" accept="image/*" name="foto_gl" id="foto_gl"
        />
    </div>
  </div>
	<script>
	 $(document).ready(function () {
			 $("#foto_gl").fileinput({
				 //showUpload:false
				 //showRemove: false,
				 language:'es',
			 });
	 });
 </script>


    <div class="row">
      <div class="col-md-6">
        <br>
        <label for="">
        <b>Latitud:</b>
      </label>
      <input type="number" name="latitud_gl" id="latitud_gl"
			value="<?php echo $corresponsalEditar->latitud_gl; ?>"
      placeholder="Ingrese el latitud..." class="form-control" readonly>

      </div>
      <div class="col-md-6">
        <br>
        <label for="">
        <b>Longitud:</b>
      </label>
      <input type="number" name="longitud_gl" id="longitud_gl"
			value="<?php echo $corresponsalEditar->longitud_gl; ?>"
      placeholder="Ingrese el longitud..." class="form-control" readonly>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div id="mapa" style="height: 250px; whidth:100%; border:1px solid black;">

      </div>
      </div>

    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-floppy-disk fa-bounce"></i> &nbsp Actualizar</button> &nbsp &nbsp
        <a href="<?php echo site_url('corresponsales/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>

      </div>

    </div>
</form>

<br>
<br>
<script type="text/javaScript">
  function initMap(){
    var coordenadaCentral =
		new google.maps.LatLng(<?php echo $corresponsalEditar->latitud_gl; ?>, <?php echo $corresponsalEditar->longitud_gl; ?>);
   var miMapa= new google.maps.Map(
     document.getElementById('mapa'),{
       center: coordenadaCentral,
       zoom: 10,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     }
   );
   var marcador= new google.maps.Marker({
     position:coordenadaCentral,
     map: miMapa,
     title: 'Seleccione la ubicacion',
     draggable:true
   });
   google.maps.event.addListener(
    marcador,
    'dragend',
    function(event){
      var latitud=this.getPosition().lat();
      var longitud=this.getPosition().lng();
      document.getElementById('latitud_gl').value=latitud;
      document.getElementById('longitud_gl').value=longitud;
    }
   );
  }

		document.getElementById('idage_gl_agencia').value = "<?php echo $cajeroEditar->idage_gl_agencia; ?>";

</script>

<script type="text/javascript">
      $.validator.addMethod(
    'lettersonly',
    function (value, element) {
      return this.optional(element) || /^[a-zA-Z\s]*$/.test(value)
    },
    'Solo se permiten letras en este campo'
  )
      $("#frm_nuevo_corresponsal").validate({
        rules:{
          "nombre_gl":{
            required:true,
            lettersonly:true
          },
          "servicios_gl":{
            required:true,
            lettersonly:true
          },
          "foto_gl":{
            required:true
          }


        },
        messages:{
          "idage_gl":{
        required:"Por favor seleccione la agencia"
       },
       "nombre_gl":{
         required:"Ingrese el nombre"
       },
       "servicios_gl":{
         required:"Ingrese el servicio"
       },
       "foto_gl":{
         required:"Ingrese la foto"
       }

      },
    });
  </script>
