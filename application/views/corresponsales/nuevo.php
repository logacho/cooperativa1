<h1>
  <b>
    <i class="fa fa-plus-circle"></i>
    NUEVO CORRESPONSAL
  </b>
</h1>
<br>
<form class="" action=<?php echo site_url('corresponsales/guardarCorresponsal') ?> method="post" enctype="multipart/form-data">
  <label for="">AGENCIAS</label>
  <select name="idage_gl_agencia" id="idage_gl_agencia" required class="form-control">
    <option value="">Seleccione la Agencia</option>
    <?php foreach ($agencias as $agencia) : ?>
      <option value="<?php echo $agencia->idage_gl; ?>">
        <?php echo $agencia->nombre_gl; ?>
      </option>
    <?php endforeach; ?>
  </select>

  <label for=""> <b>NOMBRE:</b> </label>
  <input type="text" name="nombre_gl" id="nombre_gl" class="form-control" value="" placeholder="Ingrese el nombre" oninput="this.value = this.value.replace(/[^a-zA-Z\s]/g, '')">
  <br>
  <label for=""> <b>SERVICIOS:</b> </label>
  <input type="text" name="servicios_gl" id="servicios_gl" class="form-control" value="" placeholder="Ingrese el servicio" oninput="this.value = this.value.replace(/[^a-zA-Z\s]/g, '')">
  <br>
  <label for=""> <b>FOTOGRAFIA:</b> </label>
  <input type="file" name="foto_gl" id="foto_gl" class="form-control" required accept="image/*">
  <br>
  <script>
          $(document).ready(function () {
              $("#foto_gl").fileinput({
                //showUpload:false
                //showRemove: false,
                language:'es',
              });
          });
        </script>

  <div class="row">
    <div class="col-md-6">
      <br>
      <label for=""><b>LATITUD:</b></label>
      <input type="number" name="latitud_gl" id="latitud_gl" class="form-control"  placeholder="Ingrese la latitud" readonly>
    </div>
    <div class="col-md-6">
      <br>
      <label for=""><b>LONGITUD:</b></label>
      <input type="number" name="longitud_gl" id="longitud_gl" class="form-control"  placeholder="Ingrese la longitud" readonly>
    </div>

  </div><br>
  <div class="row">
    <div class="col-md-12">
      <div id="mapa" style="height:250px; widht:100%; border:1px solid black;" ></div>

    </div>

  </div><br>

  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary"><i class="fa-regular fa-floppy-disk fa-spin"></i>&nbspGuardar&nbsp
      </button> &nbsp;  &nbsp;  &nbsp;  &nbsp;
      <a href="<?php echo site_url('corresponsales/index') ?>"class="btn btn-danger"><i class="fa-solid fa-x fa-spin"></i>&nbspCancelar&nbsp</a>
    </div>
  </div>



</form>
<br>

<script type="text/javascript">
  function initMap(){
    var coordenadaCentral=new google.maps.LatLng(-0.3323792591918456, -78.43494700389284);
    var miMapa=new google.maps.Map(
      document.getElementById('mapa'),
      {
        center:coordenadaCentral,
        zoom:8,
        mapTypeId:google.maps.MapTypeId.ROADMAP
      }
    );

    var marcador=new google.maps.Marker({
      position:coordenadaCentral,
      map:miMapa,
      title:'Selecciona la Ubicacion',
      draggable:true
    });

    google.maps.event.addListener(
      marcador,
      'dragend',
      function(event){
        var latitud=this.getPosition().lat();
        var longitud=this.getPosition().lng();
        //alert(latitud+"---"+longitud);
        document.getElementById('latitud_gl').value=latitud;
        document.getElementById('longitud_gl').value=longitud;

      }
    );
  }

</script>
<script type="text/javascript">
      $.validator.addMethod(
    'lettersonly',
    function (value, element) {
      return this.optional(element) || /^[a-zA-Z\s]*$/.test(value)
    },
    'Solo se permiten letras en este campo'
  )
      $("#frm_nuevo_corresponsal").validate({
        rules:{
          "nombre_gl":{
            required:true,
            lettersonly:true
          },
          "servicios_gl":{
            required:true,
            lettersonly:true
          },
          "foto_gl":{
            required:true
          }


        },
        messages:{
          "idage_gl":{
        required:"Por favor seleccione la agencia"
       },
       "nombre_gl":{
         required:"Ingrese el nombre"
       },
       "servicios_gl":{
         required:"Ingrese el servicio"
       },
       "foto_gl":{
         required:"Ingrese la foto"
       }

      },
    });
  </script>
