<h1><i class="fa fa-corresponsal"></i>CAJEROS</h1>

<div class="row">
  <div class="col-md-12 text-end">

    <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      <i class="fa fa-eye"></i> Ver Mapa
    </button>

     <a href="<?php echo site_url('cajeros/nuevo'); ?>" class="btn btn-outline-primary">  <i class="fa fa-plus-circle fa-1x"></i>
       Agregar Cajero</a>

  </div>

</div><br>

<?php if ($listadoCajeros): ?>
    <table class="table table-bordered">
        <thead>
              <tr>
                <th>ID</th>
                <th>ESTADO</th>
                <th>FOTOGRAFIA</th>

                <th>LATITUD</th>
                <th>LONGITUD</th>
                <th>AGENCIA</th>

                <th>Acciones</th>
              </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoCajeros as $cajero): ?>
                <tr>
                  <td><?php echo $cajero->idca_gl; ?></td>
                  <td><?php echo $cajero->estado_gl; ?></td>

                  <td>
                    <?php if ($cajero->foto_gl!=""): ?>
                      <img src="<?php echo base_url('uploads/cajeros/').$cajero->foto_gl; ?>"
                      height="100px" alt="">
                    <?php else: ?>
                      N/A
                    <?php endif; ?>
                  </td>

                  <td><?php echo $cajero->latitud_gl; ?></td>
                  <td><?php echo $cajero->longitud_gl; ?></td>
                  <td>
                        <?php
                          foreach ($agencias as $agencia) {
                            if ($agencia->idage_gl == $cajero->idage_gl_agencia) {
                                echo $agencia->nombre_gl;
                              break;
                            }
                         }
                        ?>
                  </td>


                  <td>
                    <a href="<?php echo site_url('cajeros/editar/').$cajero->idca_gl; ?>"
                       class="btn btn-warning"
                       title="Editar">
                    <i class="fa fa-pen"></i>
                  </a>
                      <a href="<?php echo site_url('cajeros/borrar/').$cajero->idca_gl; ?>" class="btn btn-danger"><i class="fa fa-trash"></i>

                      </a>
                  </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>




    <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-xl">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"> <i class="fa fa-eye"></i> Mapa de Cajeros
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <div id="reporteMapa" style="height:300px; width:100%; border:2px solid black;">


                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-bs-dismiss="modal"> <i class="fa fa-times" ></i> Cerrar</button>

              </div>
            </div>
          </div>
        </div>


        <script type="text/javascript">
              function initMap(){
                var coordenadaCentral=
                    new google.maps.LatLng(-0.152948869329262,
                      -78.4868431364856);
                var miMapa=new google.maps.Map(
                  document.getElementById('reporteMapa'),
                  {
                    center:coordenadaCentral,
                    zoom:8,
                    mapTypeId:google.maps.MapTypeId.ROADMAP
                  }
                );
                <?php foreach ($listadoCajeros as $cajero): ?>
                var coordenadaTemporal=
                    new google.maps.LatLng(
                      <?php echo $cajero->latitud_gl; ?>,
                      <?php echo $cajero->longitud_gl; ?>);
                  var marcador=new google.maps.Marker({
                    position:coordenadaTemporal,
                    map:miMapa,
                    title:'<?php echo $cajero->estado_gl; ?>',
                  });
                <?php endforeach; ?>

              }
            </script>


    <?php else: ?>
      <div class="alert alert-danger">
          No se encontro cajeros registrados
      </div>
    <?php endif; ?>
