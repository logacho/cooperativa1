<h1>EDITAR CAJERO</h1>
<form class="" method="post" action="<?php echo site_url('cajeros/actualizarCajero'); ?>" enctype="multipart/form-data">
	<input type="hidden" name="idca_gl" id="idca_gl"
	value="<?php echo $cajeroEditar->idca_gl; ?>">
  <label for="">AGENCIAS:</label>
  <select name="idage_gl_agencia" id="idage_gl_agencia" required class="form-control">
    <option value="">--Seleccione la Agencia--</option>
    <?php foreach ($agencias as $agencia) : ?>
      <option value="<?php echo $agencia->idage_gl; ?>">
        <?php echo $agencia->nombre_gl; ?>
      </option>
    <?php endforeach; ?>
  </select>
  <label for="">
    <b>ESTADO:</b>
  </label>
  <input type="text" name="estado_gl" id="estado_gl"
	value="<?php echo $cajeroEditar->estado_gl; ?>"
  placeholder="Ingrese el estado..." class="form-control" required
	oninput="this.value = this.value.replace(/[^a-zA-Z\s]/g, '')">
  <br>
  <div class="row ">
    <div class="col-md-6 mb-4">
      <label class="">Foto Actual</label>
      <br>
      <?php if (!empty($cajeroEditar->foto_gl)): ?>
        <a target="_blank" href="<?php echo base_url('uploads/cajeros/') . $cajeroEditar->foto_gl; ?>">
            <img width="150" src="<?php echo base_url('uploads/cajeros/') . $cajeroEditar->foto_gl; ?>" alt="">
          </a>
        <?php else: ?>
            <p>No hay foto</p>
      <?php endif ?>
    </div>
    <div class="col-md-6">
      <label class="">Cambiar Foto</label>
      <br>
        <input
          class="form-control"
          type="file" accept="image/*" name="foto_gl" id="foto_gl"
        />
    </div>
  </div>
	<script>
	 $(document).ready(function () {
			 $("#foto_gl").fileinput({
				 //showUpload:false
				 //showRemove: false,
				 language:'es',
			 });
	 });
 </script>



    <div class="row">
      <div class="col-md-6">
        <br>
        <label for="">
        <b>Latitud:</b>
      </label>
      <input type="number" name="latitud_gl" id="latitud_gl"
			value="<?php echo $cajeroEditar->latitud_gl; ?>"
      placeholder="Ingrese el latitud..." class="form-control" readonly>

      </div>
      <div class="col-md-6">
        <br>
        <label for="">
        <b>Longitud:</b>
      </label>
      <input type="number" name="longitud_gl" id="longitud_gl"
			value="<?php echo $cajeroEditar->longitud_gl; ?>"
      placeholder="Ingrese el longitud..." class="form-control" readonly>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div id="mapa" style="height: 250px; whidth:100%; border:1px solid black;">

      </div>
      </div>

    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-floppy-disk fa-bounce"></i> &nbsp Actualizar</button> &nbsp &nbsp
        <a href="<?php echo site_url('cajeros/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>

      </div>

    </div>
</form>

<br>
<br>
<script type="text/javaScript">
  function initMap(){
    var coordenadaCentral =
		new google.maps.LatLng(<?php echo $cajeroEditar->latitud_gl; ?>, <?php echo $cajeroEditar->longitud_gl; ?>);
   var miMapa= new google.maps.Map(
     document.getElementById('mapa'),{
       center: coordenadaCentral,
       zoom: 10,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     }
   );
   var marcador= new google.maps.Marker({
     position:coordenadaCentral,
     map: miMapa,
     title: 'Seleccione la ubicacion',
     draggable:true
   });
   google.maps.event.addListener(
    marcador,
    'dragend',
    function(event){
      var latitud=this.getPosition().lat();
      var longitud=this.getPosition().lng();
      document.getElementById('latitud_gl').value=latitud;
      document.getElementById('longitud_gl').value=longitud;
    }
   );
  }

  document.getElementById('idage_gl_agencia').value = "<?php echo $cajeroEditar->idage_gl_agencia; ?>";

</script>

<script type="text/javascript">
      $.validator.addMethod(
    'lettersonly',
    function (value, element) {
      return this.optional(element) || /^[a-zA-Z\s]*$/.test(value)
    },
    'Solo se permiten letras en este campo'
  )
      $("#frm_nuevo_cajero").validate({
        rules:{
          "estado_gl":{
            required:true,
            lettersonly:true
          },
          "foto_gl":{
            required:true
          }


        },
        messages:{
          "idage_gl":{
        required:"Por favor seleccione la agencia"
       },
       "estado_gl":{
         required:"Ingrese el estado"
       },
       "foto_gl":{
         required:"Ingrese la foto"
       }

      },
    });
  </script>
