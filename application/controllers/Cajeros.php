<?php
  /**
   *
   */
  class Cajeros extends CI_Controller
  {

    function __construct()
    {
      parent::__construct();
      $this->load->model('Cajero');
      $this->load->model('Agencia');

      //phpinfo();

    }

    //renderizacionde la vista cajeros
    public function index(){
      $data["listadoCajeros"]=$this->Cajero->consultarTodos();
      $data["agencias"]=$this->Agencia->consultarTodos();
      $this->load->view("header");
      $this->load->view("cajeros/index",$data);
      $this->load->view("footer");
    }
    // eliminacion del cajeros recibiendo el id por el metodo get

    public function borrar($idca_gl){
      $this->Cajero->eliminar($idca_gl);
      $this->session->set_flashdata("confirmacion", "Cajero eliminado exitosamente");
      redirect('cajeros/index');
  }

  //Rendirizacion del formulario de nuevo cajero
    public function nuevo(){
      $data["agencias"]=$this->Agencia->consultarTodos();
      $this->load->view("header");
      $this->load->view("cajeros/nuevo", $data);
      $this->load->view("footer");
    }

    public function guardarCajero(){

      /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/
      $config['upload_path']=APPPATH.'../uploads/cajeros/'; //ruta de subida de archivos
      $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
      $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
      $nombre_aleatorio="cajero_".time()*rand(100,10000);//creando un nombre aleatorio
      $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
      $this->load->library('upload',$config);//cargando la libreria UPLOAD
      if($this->upload->do_upload("foto_gl")){ //intentando subir el archivo
         $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
         $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
      }else{
        $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
      }

      $datosNuevoCajero=array(
        "estado_gl"=>$this->input->post("estado_gl"),
        "foto_gl"=>$nombre_archivo_subido,
        "latitud_gl"=>$this->input->post("latitud_gl"),
        "longitud_gl"=>$this->input->post("longitud_gl"),
        "idage_gl_agencia"=>$this->input->post("idage_gl_agencia")

      );

      $this->Cajero->insertar($datosNuevoCajero);
      $this->session->set_flashdata("confirmacion", "Cajero guardado exitosamente");
      enviarEmail("angelica.logacho8427@utc.edu.ec","CREACION",
          "<h1>SE CREO EL Cajero </h1>".$datosNuevoCajero['estado_gl']);
      redirect('cajeros/index');
    }
    public function editar($id){
      $data["cajeroEditar"]=$this->Cajero->obtenerPorId($id);
      $data["agencias"]=$this->Agencia->consultarTodos();
      $this->load->view("header");
      $this->load->view("cajeros/editar",$data);
      $this->load->view("footer");
    }
    /*public function actualizarCajero(){
      $idca_gl=$this->input->post("idca_gl");
      $datosCajero=array(
        "estado_gl"=>$this->input->post("estado_gl"),
        "foto_gl"=>$nombre_archivo_subido,
        "latitud_gl"=>$this->input->post("latitud_gl"),
        "longitud_gl"=>$this->input->post("longitud_gl")
      );
      $this->Cajero->actualizar($idca_gl,$datosCajero);
      $this->session->set_flashdata("confirmacion",
      "Cajero actualizado exitosamente");
      redirect('cajeros/index');
    }*/

    public function actualizarCajero()
   {
       $idca_gl = $this->input->post("idca_gl");

       $cajeroEditar = $this->Cajero->obtenerPorId($idca_gl);

       // Configuración de carga de archivos
       $config['upload_path'] = APPPATH . '../uploads/cajeros/';
       $config['allowed_types'] = 'gif|jpg|png';
       $config['max_size'] = 5000; // 5MB

       if (!empty($_FILES['foto_gl']['name'])) {
           $this->load->library('upload', $config);
           if ($this->upload->do_upload('foto_gl')) {
               $dataArchivoSubido = $this->upload->data();
               $nombre_archivo_subido = $dataArchivoSubido['file_name'];

               // Eliminar la imagen anterior si existe
               if (!empty($cajeroEditar->foto_gl)) {
                   unlink($config['upload_path'] . $cajeroEditar->foto_gl);
               }
           } else {
               // Manejar errores de carga de archivo según sea necesario
               $error = array('error' => $this->upload->display_errors());
               print_r($error);
               return;
           }
       } else {
           // Si no se está cargando una nueva imagen, mantener la imagen existente
           $nombre_archivo_subido = $cajeroEditar->foto_gl;
       }

       // Datos del cajero para actualizar
       $datosCajero = array(
         "estado_gl"=>$this->input->post("estado_gl"),
         "foto_gl"=>$nombre_archivo_subido,
         "latitud_gl"=>$this->input->post("latitud_gl"),
         "longitud_gl"=>$this->input->post("longitud_gl"),
         "idage_gl_agencia"=>$this->input->post("idage_gl_agencia")
       );

       // Actualizar el Cajero
       $this->Cajero->actualizar($idca_gl, $datosCajero);
       $this->session->set_flashdata('confirmacion', 'El cajero fue actualizado correctamente');
       redirect("cajeros/index");
   }

  }





 ?>
