<?php
class Cooperativas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Cooperativa");
        // Deshabilitando errores y advertencias de PHP
        error_reporting(0);
    }

    public function index()
    {
        $data["listadoCooperativas"] = $this->Cooperativa->consultarTodos();
        $this->load->view("header");
        $this->load->view("cooperativas/index", $data);
        $this->load->view("footer");
    }

    public function borrar($idco_gl)
    {
        $this->Cooperativa->eliminar($idco_gl);
        $this->session->set_flashdata("confirmacion", "Cooperativa eliminada existosamente");
        redirect('cooperativas/index');
    }

    // Renderizando hospitales
    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("cooperativas/nuevo");
        $this->load->view("footer");
    }

    // Insertar hospitales
    public function guardarCooperativa()
    {
        $datosNuevosCooperativa = array(
            "nombre_gl" => $this->input->post('nombre_gl'),
            "telefono_gl" => $this->input->post('telefono_gl'),
            "correo_gl" => $this->input->post('correo_gl'),
            "latitud_gl" => $this->input->post('latitud_gl'),
            "longitud_gl" => $this->input->post('longitud_gl'),
            "mision_gl" => $this->input->post("mision_gl"),
            "vision_gl" => $this->input->post("vision_gl"),
            "historia_gl" => $this->input->post("historia_gl")
        );
        $this->Cooperativa->insertar($datosNuevosCooperativa);
        $this->session->set_flashdata("confirmacion", "Cooperativa creada exitosamente");
        redirect('cooperativas/index');
    }

    public function editar($id)
    {
        $data["cooperativaEditar"] = $this->Cooperativa->obtenerPorId($id);
        $this->load->view("header");
        $this->load->view("cooperativas/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarCooperativa()
    {
        $idco_gl = $this->input->post("idco_gl");
        $datosCooperativa = array(
            "nombre_gl" => $this->input->post("nombre_gl"),
            "telefono_gl" => $this->input->post("telefono_gl"),
            "correo_gl" => $this->input->post("correo_gl"),
            "latitud_gl" => $this->input->post("latitud_gl"),
            "longitud_gl" => $this->input->post("longitud_gl"),
            "mision_gl" => $this->input->post("mision_gl"),
            "vision_gl" => $this->input->post("vision_gl"),
            "historia_gl" => $this->input->post("historia_gl")
        );
        $this->Cooperativa->actualizar($idco_gl, $datosCooperativa);
        $this->session->set_flashdata("confirmacion", "Cooperativa actualizada exitosamente");
        redirect('cooperativas/index');
    }
}
?>
