<?php

class Agencias extends CI_Controller
{


  function __construct()
    {
        parent::__construct();
        $this->load->model("Agencia");
        $this->load->model("Cooperativa"); // Carga el modelo Cooperativa
        // Deshabilitando errores y advertencias de PHP
        error_reporting(0);
    }

  public function index(){
    $data["listadoAgencias"] = $this->Agencia->consultarTodos();
    $this->load->view("header");
    $this->load->view("agencias/index", $data);
    $this->load->view("footer");
  }

  public function borrar($idco_gl){
    $this->Agencia->eliminar($idco_gl);
    $this->session->set_flashdata("confirmacion", "Agencia eliminada exitosamente");
    redirect('agencias/index');
  }

  // Renderizando agencias
  public function nuevo(){
    $this->load->model("Agencia");
    $data["cooperativa"] = $this->Agencia->obtenerCooperativa();
    $this->load->view("header");
    $this->load->view("agencias/nuevo", $data);
    $this->load->view("footer");
  }
  // Insertar agencias
  public function guardarAgencia()
{
    $datosNuevosAgencia = array(
        "idco_gl_cooperativa" => $this->input->post('idco_gl_cooperativa'),
        "nombre_gl" => $this->input->post('nombre_gl'),
        "telefono_gl" => $this->input->post('telefono_gl'),
        "gerente_gl" => $this->input->post('gerente_gl'),
        "latitud_gl" => $this->input->post('latitud_gl'),
        "longitud_gl" => $this->input->post('longitud_gl')
    );
    $this->Agencia->insertar($datosNuevosAgencia);
    $this->session->set_flashdata("confirmacion", "Agencia creada exitosamente");
    redirect('agencias/index');
}



  // Editar agencia
  public function editar($id) {
    $data["agenciaEditar"] = $this->Agencia->obtenerPorId($id);
    $data["cooperativas"] = $this->Agencia->obtenerCooperativa(); // Renombramos la variable a $cooperativas
    $this->load->view("header");
    $this->load->view("agencias/editar", $data);
    $this->load->view("footer");
}


  // Actualizar agencia
  public function actualizarAgencia(){
    $idco_gl = $this->input->post("idco_gl");
    $datosAgencia = array(
      "nombre_gl" => $this->input->post("nombre_gl"),
      "telefono_gl" => $this->input->post("telefono_gl"),
      "gerente_gl" => $this->input->post("gerente_gl"),
      "latitud_gl" => $this->input->post("latitud_gl"),
      "longitud_gl" => $this->input->post("longitud_gl"),
      "idco_gl_cooperativa" => $this->input->post('idco_gl_cooperativa')
    );

    $this->Agencia->actualizar($idco_gl, $datosAgencia);
    $this->session->set_flashdata("confirmacion", "Agencia actualizada exitosamente");
    redirect('agencias/index');
  }

  public function mostrar_mapa() {
      // Obtener el listado de agencias
      $data['listadoAgencias'] = $this->Agencia->consultarTodos();

      // Cargar la vista con los datos
      $this->load->view("header");
      $this->load->view("agencias1", $data);
      $this->load->view("footer");
  }


}

?>
