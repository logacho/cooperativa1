<?php
  /**
   *
   */
  class Corresponsales extends CI_Controller
  {

    function __construct()
    {
      parent::__construct();
      $this->load->model('Corresponsal');
      $this->load->model('Agencia');

      //phpinfo();

    }

    //renderizacionde la vista corresponsales
    public function index(){
      $data["listadoCorresponsales"]=$this->Corresponsal->consultarTodos();
      $data["agencias"]=$this->Agencia->consultarTodos();
      $this->load->view("header");
      $this->load->view("corresponsales/index",$data);
      $this->load->view("footer");
    }
    // eliminacion del corresponsales recibiendo el id por el metodo get

    public function borrar($idcor_gl){
      $this->Corresponsal->eliminar($idcor_gl);
      $this->session->set_flashdata("confirmacion", "Corresponsal eliminado exitosamente");
      redirect('corresponsales/index');
  }



  //Rendirizacion del formulario de nuevo corresponsal
    public function nuevo(){
      $data["agencias"]=$this->Agencia->consultarTodos();
      $this->load->view("header");
      $this->load->view("corresponsales/nuevo");
      $this->load->view("footer");
    }

    public function guardarCorresponsal(){

      /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/
      $config['upload_path']=APPPATH.'../uploads/corresponsales/'; //ruta de subida de archivos
      $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
      $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
      $nombre_aleatorio="corresponsal_".time()*rand(100,10000);//creando un nombre aleatorio
      $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
      $this->load->library('upload',$config);//cargando la libreria UPLOAD
      if($this->upload->do_upload("foto_gl")){ //intentando subir el archivo
         $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
         $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
      }else{
        $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
      }

      $datosNuevoCorresponsal=array(
        "nombre_gl"=>$this->input->post("nombre_gl"),
        "servicios_gl"=>$this->input->post("servicios_gl"),
        "latitud_gl"=>$this->input->post("latitud_gl"),
        "longitud_gl"=>$this->input->post("longitud_gl"),
        "foto_gl"=>$nombre_archivo_subido,
        "idage_gl_agencia"=>$this->input->post("idage_gl_agencia")
      );

      $this->Corresponsal->insertar($datosNuevoCorresponsal);
      $this->session->set_flashdata("confirmacion", "Corresponsal guardado exitosamente");
      enviarEmail("angelica.logacho8427@utc.edu.ec","CREACION",
          "<h1>SE CREO EL Corresponsal </h1>".$datosNuevoCorresponsal['nombre_gl']);
      redirect('corresponsales/index');
    }
    public function editar($id){
      $data["corresponsalEditar"]=$this->Corresponsal->obtenerPorId($id);
      $data["agencias"]=$this->Agencia->consultarTodos();
      $this->load->view("header");
      $this->load->view("corresponsales/editar",$data);
      $this->load->view("footer");
    }
    /*public function actualizarCorresponsal(){
      $idcor_gl=$this->input->post("idcor_gl");
      $datosCorresponsal=array(
        "nombre_gl"=>$this->input->post("nombre_gl"),
        "servicios_gl"=>$this->input->post("servicios_gl"),
        "latitud_gl"=>$this->input->post("latitud_gl"),
        "longitud_gl"=>$this->input->post("longitud_gl")
      );
      $this->Corresponsal->actualizar($idcor_gl,$datosCorresponsal);
      $this->session->set_flashdata("confirmacion",
      "Corresponsal actualizado exitosamente");
      redirect('corresponsales/index');
    }*/

    public function actualizarCorresponsal()
   {
       $idcor_gl = $this->input->post("idcor_gl");

       $corresponsalEditar = $this->Corresponsal->obtenerPorId($idcor_gl);

       // Configuración de carga de archivos
       $config['upload_path'] = APPPATH . '../uploads/corresponsales/';
       $config['allowed_types'] = 'gif|jpg|png';
       $config['max_size'] = 5000; // 5MB

       if (!empty($_FILES['foto_gl']['name'])) {
           $this->load->library('upload', $config);
           if ($this->upload->do_upload('foto_gl')) {
               $dataArchivoSubido = $this->upload->data();
               $nombre_archivo_subido = $dataArchivoSubido['file_name'];

               // Eliminar la imagen anterior si existe
               if (!empty($corresponsalEditar->foto_gl)) {
                   unlink($config['upload_path'] . $corresponsalEditar->foto_gl);
               }
           } else {
               // Manejar errores de carga de archivo según sea necesario
               $error = array('error' => $this->upload->display_errors());
               print_r($error);
               return;
           }
       } else {
           // Si no se está cargando una nueva imagen, mantener la imagen existente
           $nombre_archivo_subido = $corresponsalEditar->foto_gl;
       }

       // Datos del cajero para actualizar
       $datosCorresponsal = array(
         "estado_gl"=>$this->input->post("estado_gl"),
         "foto_gl"=>$nombre_archivo_subido,
         "latitud_gl"=>$this->input->post("latitud_gl"),
         "longitud_gl"=>$this->input->post("longitud_gl"),
         "idage_gl_agencia"=>$this->input->post("idage_gl_agencia")
       );

       // Actualizar el Cajero
       $this->Cajero->actualizar($idca_gl, $datosCajero);
       $this->session->set_flashdata('confirmacion', 'El cajero fue actualizado correctamente');
       redirect("cajeros/index");
   }

  }





 ?>
